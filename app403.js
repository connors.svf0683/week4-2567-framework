//Define Object
const users = [
    {
        "fname" : "Jimmy",
        "lname" : "Seesang",
        "age" : 19,
        "address" : "15th Park Avenue"
    },
    {
        "fname" : "John",
        "lname" : "convent",
        "age" : 12,
        "address" : "12th London"
    },
    {
        "fname" : "Janny",
        "lname" : "convent",
        "age" : 12,
        "address" : "12th London"
    }
]
user_1 = users[0]
console.log(typeof(user_1))
console.log(user_1.fname)

for (const user of users) {
    console.log(
        `Name : ${user.fname} Address : ${user.lname} Age : ${user.age} Address : ${user.address}`
    )
}

const drinks = ["Coke", "Pepsi", "Fanta", "Orange"]

for (drink in drinks) {
    console.log(drinks[drink])
}