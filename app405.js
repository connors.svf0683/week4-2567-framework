const a1 = [1, 2, 3]
const a2 = [4, 5, 6]
const arrC = [...a1, ...a2]
const user_name = {
    "fname" : "Jimmy",
    "lname" : "Seesang"
}
const user_info = {
    "age" : 19,
    "address" : "15th"
}

user = {...user_name, ...user_info}
console.log(arrC)
console.log(user)