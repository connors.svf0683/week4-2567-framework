const obj = {
    "a" : 1,
    "b" : {
        "b1" : 2.1
    },
    "c" : 3,
}

console.log(obj.b.b1)

const odd = [1, 3, 5]
const odd1 = [7, 9, 11]
const combinded = [...odd, 5, 7, ...odd1]
console.log(combinded)

function sum(a,b,...args) {
    console.log(args)
}

sum(1, 2, 3, 4, 5)